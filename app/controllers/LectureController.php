<?php
use Carbon\Carbon;
class LectureController extends BaseController {

	public function get_new()
	{
		$presenter = array('' => 'Select Presenter') + Presenter::select(DB::raw('concat (first_name, " ", last_name) as full_name, id'))->lists('full_name', 'id');
		$room = array('' => 'Select Room') + Room::select(DB::raw('concat (label) as label, id'))->lists('label', 'id');
		return View::make('lecture/new')->with('title', 'Create Lecture')->with('presenter', $presenter)->with('room', $room);
	}

	public function post_create()
	{
		$validation = Lecture::validate(Input::all());
		if ($validation->fails()) {
			return Redirect::to('lecture/new')->withErrors($validation)->withInput();
		} else {
			$l = Lecture::create(array(
				'title' 		=> Input::get('title'),
				'organizer_id' 	=> Input::get('organizer_id'),
				'presenter_id' 	=> Input::get('presenter_id'),
				'room_id' 		=> Input::get('room_id'),
				// 'start_date' 	=> Input::get('start_date'),
				// 'end_date'		=> Input::get('end_date'),
				'start_time'	=> Input::get('start_time'),
				'end_time'		=> Input::get('end_time'),
				'overview' 		=> Input::get('overview')
			));
			return Redirect::to('lectures')->with('message', 'Lecture was created successfully');
		}
	}

	public function view($id = null)
	{
		$lecture = Lecture::find($id);
		$lecture_delegate = 0;
		$lecture_delegate = DB::table('lecture_delegates')->where('lecture_id', $id)->count();

		// $room_capacity = $lecture->room->capacity;
		$newDate =  new Carbon();
		$lectureDate = Lecture::where('start_time', '=', $lecture->start_time)->where('start_time', '>', $newDate)->get();		

		return View::make('lecture.view')->with('lecture', $lecture)->with('title', 'Apply lecture')
				->with('lecture_delegate', $lecture_delegate)->with('lectureDate', $lectureDate);
	}

	public function index()
	{
		$lectures = Lecture::all();
		return View::make('lecture/index')->with('title', 'Lecture List')->with('lectures', $lectures);
	}

	public function edit($id)
	{
		$lecture = Lecture::find($id);
		$presenter = array('' => 'Select Presenter') + Presenter::select(DB::raw('concat (first_name, " ", last_name) as full_name, id'))->lists('full_name', 'id');
		$room = array('' => 'Select Room') + Room::select(DB::raw('concat (label) as label, id'))->lists('label', 'id');
		
		return View::make('lecture/edit')->with('lecture', $lecture)->with('title', 'Edit lecture')->with('presenter', $presenter)->with('room', $room);
	}

	public function update($id)
	{
		$lecture = Lecture::find($id);

		$data = Input::all();

		$lecture->title = $data['title'];
		$lecture->organizer_id = $data['organizer_id'];
		$lecture->presenter_id = $data['presenter_id'];
		$lecture->room_id = $data['room_id'];
		$lecture->start_time = $data['start_time'];
		$lecture->end_time = $data['end_time'];
		$lecture->overview = $data['overview'];
       
        $lecture->save();
        
        return Redirect::to('lectures');
	}
}
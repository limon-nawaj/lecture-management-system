<?php

class Static_Controller extends BaseController {

	public function get_index()
	{
		$staffs = Staff::all();
		$lectures = Lecture::orderBy('start_time', 'DESC')->take(10)->get();
		return View::make('static/index')->with('title', 'home')->with('lectures', $lectures)->with('staffs', $staffs);
	}

	public function more_lecture()
	{

		$lectures = Lecture::orderBy('start_time', 'DESC')->get();
		return View::make('static/lectures')->with('title', 'Lecture List')->with('lectures', $lectures);
	}

}
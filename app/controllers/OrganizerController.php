<?php 

/**
* 
*/
class Organizer_Controller extends BaseController
{
	public function get_new()
	{
		return View::make('organizer/new')->with('title', 'Add new Organizer');
	}

	public function post_create()
	{
		$validation = User::validate(Input::all());
		$v = Organizer::validate(Input::all());
		if ($validation->fails() || $v->fails()) {
			return Redirect::to('organizer/new')->withErrors($validation)->withInput();
		} else {
			$u = User::create(array(
				'username' => Input::get('username'),
				'password' => Hash::make(Input::get('password')),
				'role' => Input::get('role')
			));
			if ($u->save()) {
				
				if ($v->fails()) {
					return Redirect::to('organizer/new')->withErrors($validation)->withInput();
				}else{
					Organizer::create(array(
						'user_id' => $u->id,
						'first_name' => Input::get('first_name'),
						'last_name' => Input::get('last_name'),
						'email' => Input::get('email'),
						'phone' => Input::get('phone'),
						'address' => Input::get('address')
					));
				}
			}
			return Redirect::to('organizers')->with('message', 'User was created successfully');
		}
	}

	public function index()
	{
		$organizers = Organizer::all();
		return View::make('organizer/index')->with('title', 'Organizer List')->with('organizers', $organizers);
	}

	public function edit($id)
	{
		$organizer = Organizer::find($id);
		return View::make('organizer/edit')->with('organizer', $organizer)->with('title', 'Edit Organizer');
	}

	public function update($id)
	{
		$organizer = Organizer::find($id);

		$data = Input::all();


		$organizer->first_name = $data['first_name'];
		$organizer->last_name = $data['last_name'];
		$organizer->email = $data['email'];
		$organizer->phone = $data['phone'];
		$organizer->address = $data['address'];
        
        
        $organizer->save();
        
        return Redirect::to('organizers');
	}
}
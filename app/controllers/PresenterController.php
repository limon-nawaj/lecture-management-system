<?php 

/**
* 
*/
class Presenter_Controller extends BaseController
{
	public function get_new()
	{
		return View::make('presenter/new')->with('title', 'Add new presenter');
	}

	public function post_create()
	{
		$validation = User::validate(Input::all());
		if ($validation->fails()) {
			return Redirect::to('presenter/new')->withErrors($validation)->withInput();
		} else {
			$u = User::create(array(
				'username' => Input::get('username'),
				'password' => Hash::make(Input::get('password')),
				'role' => Input::get('role')
			));
			if ($u->save()) {
				$v = Presenter::validate(Input::all());
				if ($v->fails()) {
					return Redirect::to('presenter/new')->withErrors($validation)->withInput();
				}else{

					Presenter::create(array(
						'user_id' => $u->id,
						'first_name' => Input::get('first_name'),
						'last_name' => Input::get('last_name'),
						'email' => Input::get('email'),
						'phone' => Input::get('phone'),
						'address' => Input::get('address')
					));
				}
			}
			return Redirect::to('presenters')->with('message', 'User was created successfully');
		}
	}

	public function index()
	{
		$presenters = Presenter::all();
		return View::make('presenter/index')->with('title', 'presenter List')->with('presenters', $presenters);
	}

	public function edit($id)
	{
		$presenter = Presenter::find($id);
		return View::make('presenter/edit')->with('presenter', $presenter)->with('title', 'Edit presenter');
	}

	public function update($id)
	{
		$presenter = Presenter::find($id);

		$data = Input::all();


		$presenter->first_name = $data['first_name'];
		$presenter->last_name = $data['last_name'];
		$presenter->email = $data['email'];
		$presenter->phone = $data['phone'];
		$presenter->address = $data['address'];
        
        
        $presenter->save();
        
        return Redirect::to('presenters');
	}
}
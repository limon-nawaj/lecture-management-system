<?php 

/**
* 
*/
class RoomController extends BaseController
{
	
	public function get_new()
	{
		return View::make('room/new')->with('title', 'New Room');
	}

	public function post_create()
	{
		$validation = Room::validate(Input::all());
		if ($validation->fails()) {
			return Redirect::to('room/new')->withErrors($validation)->withInput();
		} else {
			Room::create(array(
				'label' => Input::get('label'),
				'capacity' => Input::get('capacity'),
				'address' => Input::get('address'),
				'building' => Input::get('building')
			));
			return Redirect::to('rooms')->with('message', 'Room was created successfully');
		}
		
	}

	public function index()
	{
		$rooms = Room::all();
		return View::make('room/index')->with('title', 'Room List')->with('rooms', $rooms);
	}

	public function edit($id)
	{
		$room = Room::find($id);
		return View::make('room/edit')->with('room', $room)->with('title', 'Edit room');
	}

	public function update($id)
	{
		$room = Room::find($id);

		$data = Input::all();


		$room->label = $data['label'];
		$room->capacity = $data['capacity'];
		$room->building = $data['building'];
		$room->address = $data['address'];
        
        
        $room->save();
        
        return Redirect::to('rooms');
	}
}
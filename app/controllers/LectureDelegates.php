<?php

use Carbon\Carbon;


class LectureDelegatesController extends BaseController {
	
	
	public function create()
	{
		$lecture_id = Input::get('lecture_id');
		$delegate_id = Input::get('delegate_id');
		$lecture = Lecture::find($lecture_id);
		
		
		LectureDelegate::create(array(
			'lecture_id' => Input::get('lecture_id'),
			'delegate_id'=> Input::get('delegate_id')
		));
		return Redirect::to('/')->with('message', 'Your are successfully apply to the lecture');
		
	}
}


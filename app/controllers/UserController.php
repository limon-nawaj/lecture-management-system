<?php 

/**
* 
*/
class User_Controller extends BaseController
{
	public function get_login()
	{
		return View::make('users.login')->with('title', 'Login');
	}

	public function post_login(){
		$user = array('username'=>Input::get('username'), 'password'=>Input::get('password'));

		if (Auth::attempt($user)) {
			return Redirect::route('home')->with('message', 'Login successfully'); 
		}else{
			return Redirect::route('user_login')->with('message', 'your username and password was invalid')->withInput();
		}
	}

	public function get_logout(){
		if (Auth::check()) {
			Auth::logout();
			return Redirect::route('home')->with('message', 'You are successfully logout'); 
		}
	}
}
<?php 


class Admin_Controller extends BaseController
{
	
	public function get_registration()
	{
		$staffs = User::all();
		return View::make('admin/new')->with('title', 'Registration')->with('staffs', $staffs);
	}

	public function post_create()
	{
		$validation = User::validate(Input::all());
		if ($validation->fails()) {
			return Redirect::to('staff/new')->withErrors($validation)->withInput();
		} else {
			$u = User::create(array(
				'username' => Input::get('username'),
				'password' => Hash::make(Input::get('password')),
				'role' => Input::get('role')
			));
			if ($u->save()) {
				$v = Staff::validate(Input::all());
				if ($v->fails()) {
				}else{

					Staff::create(array(
						'user_id' => $u->id,
						'first_name' => Input::get('first_name'),
						'last_name' => Input::get('last_name'),
						'email' => Input::get('email'),
						'phone' => Input::get('phone'),
						'address' => Input::get('address')
					));
				}
			}
			$user = User::where('username', Input::get('username'))->first();
			Auth::login($user);
			return Redirect::to('/')->with('message', 'User was created successfully');
		}
		
	}

	public function staffs()
	{
		$staffs = Staff::all();
		return View::make('admin/index')->with('title', 'All staff')->with('staffs', $staffs);
	}

	public function edit_staff($id)
	{
		$staff = Staff::find($id);
		$user = DB::table('users')->where('id', '=', $staff->user_id);
		return View::make('admin/edit')->with('staff', $staff)->with('title', 'Edit Staff');
	}

	public function update_staff($id)
	{
		$staff = Staff::find($id);
		// $user = DB::table('users')->where('id', '=', $staff->user_id);

		$data = Input::all();

		// $user->username = $data['username'];
		// $user->password = $data['password'];
		// $user->role 	= $data['role'];

		$staff->first_name = $data['first_name'];
		$staff->last_name = $data['last_name'];
		$staff->email = $data['email'];
		$staff->phone = $data['phone'];
		$staff->address = $data['address'];
        
        
        $staff->save();
        
        return Redirect::to('staffs');
	}

	public function destroy($id)
    {
        $staff = Staff::find($id);
        $user = DB::table('users')->where('id', '=', $staff->user_id);
        $user->delete();
        $staff->delete();
        return Redirect::route('staffs');
    }

}
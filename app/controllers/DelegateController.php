<?php 

/**
* 
*/
class Delegate_Controller extends BaseController
{
	public function get_new()
	{
		return View::make('delegates/new')->with('title', 'Delegates registration');
	}

	public function post_create()
	{
		$validation = User::validate(Input::all());
		if ($validation->fails()) {
			return Redirect::to('delegate/new')->withErrors($validation)->withInput();
		} else {
			$u = User::create(array(
				'username' => Input::get('username'),
				'password' => Hash::make(Input::get('password')),
				'role' => Input::get('role')
			));

			if ($u->save()) {
				$v = Delegate::validate(Input::all());
				if ($v->fails()) {
					return Redirect::to('delegate/new')->withErrors($validation)->withInput();
				}else{
					Delegate::create(array(
						'user_id' 		=> $u->id,
						'first_name' 	=> Input::get('first_name'),
						'last_name' 	=> Input::get('last_name'),
						'email' 		=> Input::get('email'),
						'phone' 		=> Input::get('phone'),
						'address' 		=> Input::get('address')
					));
				}
			}
			$user = User::where('username', Input::get('username'))->first();
			Auth::login($user);
			return Redirect::to('/')->with('message', 'User was created successfully');
		}
	}
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//static page controller
Route::get('/', 				array('as'=>'home', 'uses'=>'Static_Controller@get_index'));

//admin controller
Route::get('staff/new', 		array('as'=>'staff_registration', 		'uses'=>'Admin_Controller@get_registration'));
Route::post('staff/new', 		array('as' => 'create_staff', 			'uses' => 'Admin_Controller@post_create'));
Route::get('staffs', 			array('as' => 'staffs', 				'uses' => 'Admin_Controller@staffs'));
Route::get('staff/{id}', 		array('as' => 'edit_staff', 			'uses' => 'Admin_Controller@edit_staff'));
Route::put('staff/{id}', 		array('as' => 'update_staff', 			'uses' => 'Admin_Controller@update_staff'));

//room controller
Route::get('room/new', 			array('as'=>'new_room', 				'uses' => 'RoomController@get_new'));
Route::post('room/new', 		array('as'=>'create_room', 				'uses' => 'RoomController@post_create'));
Route::get('rooms', 			array('as'=>'rooms', 					'uses'=>'RoomController@index'));
Route::get('room/{id}', 		array('as' => 'edit_room', 				'uses' => 'RoomController@edit'));
Route::put('room/{id}', 		array('as' => 'update_room', 			'uses' => 'RoomController@update'));


//user controller
Route::get('user/login', 		array('as' => 'login', 					'uses' => 'User_Controller@get_login'));
Route::get('user/logout', 		array('as' => 'logout', 				'uses' => 'User_Controller@get_logout'));
Route::post('user/login', 		array('as' => 'user_login',				'uses' => 'User_Controller@post_login'));

//delegate controller
Route::get('delegate/new', 		array('as' => 'new_delegate', 			'uses' => 'Delegate_Controller@get_new'));
Route::post('delegate/new', 	array('as' => 'create_delegate', 		'uses' => 'Delegate_Controller@post_create'));

//organizer controller
Route::get('organizer/new', 	array('as'=>'add_organizer', 			'uses'=>'Organizer_Controller@get_new'));
Route::post('organizer/new', 	array('as' => 'create_organizer', 		'uses' => 'Organizer_Controller@post_create'));
Route::get('organizers', 		array('as'=>'organizers', 				'uses'=>'Organizer_Controller@index'));
Route::get('organizer/{id}', 	array('as' => 'edit_organizer', 		'uses' => 'Organizer_Controller@edit'));
Route::put('organizer/{id}', 	array('as' => 'update_organizer', 		'uses' => 'Organizer_Controller@update'));


//prenseter controller
Route::get('presenter/new', 	array('as'=>'add_presenter', 			'uses'=>'Presenter_Controller@get_new'));
Route::post('presenter/new', 	array('as' => 'create_presenter', 		'uses' => 'Presenter_Controller@post_create'));
Route::get('presenters', 		array('as'=>'presenters', 				'uses'=>'Presenter_Controller@index'));
Route::get('presenter/{id}', 	array('as' => 'edit_presenter', 		'uses' => 'Presenter_Controller@edit'));
Route::put('presenter/{id}', 	array('as' => 'update_presenter', 		'uses' => 'Presenter_Controller@update'));


//lecture controller
Route::get('lecture/new', 		array('as' => 'add_lecture', 			'uses' => 'LectureController@get_new'));
Route::post('lecture/new', 		array('as' => 'create_lecture', 		'uses' => 'LectureController@post_create'));
Route::get('lecture/{single}', 	array('as' => 'single_lecture', 		'uses' => 'LectureController@view'));

Route::get('lectures', 			array('as'=>'lectures', 				'uses'=>'LectureController@index'));
Route::get('lecture/{id}', 		array('as' => 'edit_lecture', 			'uses' => 'LectureController@edit'));
Route::put('lecture/{id}', 		array('as' => 'update_lecture', 		'uses' => 'LectureController@update'));



Route::get('more_lecture', 		array('as' => 'more_lecture', 			'uses' => 'Static_Controller@more_lecture'));


// lecture_delegate
// Route::get('lecturedelegate/new', 		array('as' => 'add_lecture', 	'uses' => 'LectureDelegatesController@get_new'));
Route::post('lecturedelegate/new', 		array('as' => 'create_lecture_delegate', 'uses' => 'LectureDelegatesController@create'));

@extends('commons.default')

@section('content')
	@foreach($lectures as $lecture)
		<div class="well">
			<h2>{{ $lecture->title }}</h2>
			Start date: <i>{{ $lecture->start_time }}</i> End date: <i>{{ $lecture->end_time }}</i> <br>
			<p>{{ $lecture->overview }}</p>

			{{ HTML::linkRoute('lecture', 'see more', $lecture->id) }}

		</div>
    @endforeach
@endsection
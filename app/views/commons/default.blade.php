<html>
<head>
	<title>{{ $title }}</title>
	{{ HTML::style('') }}
	{{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap-responsive.css') }}
    {{ HTML::style('css/carousel.css') }}
</head>
<body>

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          {{ HTML::linkRoute('home', 'Lecture Management System') }}
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>{{ HTML::linkRoute('home', 'Home') }}</li>

			
			<!-- {{ HTML::linkRoute('staff_registration', 'Admin Registration')}} -->

			@if(Auth::check())

				@if(Auth::user()->role=='admin' || Auth::user()->role=='organizer')
					<li>{{ HTML::linkRoute('lectures', 'Lectures') }}</li>
				@endif

				@if(Auth::user()->role=='admin')
					<li>{{ HTML::linkRoute('staffs', 'Staffs') }}</li>
					<li>{{ HTML::linkRoute('rooms', 'Rooms') }}</li>
					<li>{{ HTML::linkRoute('organizers', 'Organizers') }}</li>
					<li>{{ HTML::linkRoute('presenters', 'presenters') }}</li>
					
				@elseif(Auth::user()->role=='organizer')
					<li>{{ HTML::linkRoute('add_lecture', 'Add Lecture') }}</li>
				@endif
				<li>{{ HTML::linkRoute('logout', 'Logout') }}</li>


			@else
				<li>{{ HTML::linkRoute('new_delegate', 'Delegate Registration') }}</li>

				<li>{{ HTML::linkRoute('login', 'Login') }}</li>
			@endif


          </ul>
          
        </div>
      </div>
    </div>

    <div style="margin-top: 50px;">
    	@if(Session::has('message'))
			<p id="message">{{ Session::get('message') }}</p>
		@endif
		
		@yield('content')
    </div>
	
	{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/carousel.js') }}
</body>
</html>
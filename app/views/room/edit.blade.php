@extends('commons.default')

@section('content')
	<h2>Edit Room</h2>
	{{ Form::model($room, array('method' => 'put', 'route' => array('update_room', $room->id))) }}
     	<p>
            {{ Form::label('label', 'Room label') }} <br />
            {{ Form::text('label', Input::old('label')) }}
        </p>

        <p>
            {{ Form::label('capacity', 'capacity') }} <br />
            {{ Form::text('capacity'), Input::old('capacity') }}
        </p>

        <p>
            {{ Form::label('address', 'Address') }} <br />
            {{ Form::text('address'), Input::old('address') }}
        </p>

        <p>
            {{ Form::label('building', 'Building') }} <br />
            {{ Form::text('building'), Input::old('building') }}
        </p>

        <p>{{ Form::submit('Add room') }}</p>
     {{ Form::close() }}
@endsection
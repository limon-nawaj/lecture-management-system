@extends('commons.default')

@section('content')
	<div style="padding: 10px;">
		{{ HTML::linkRoute('new_room', 'Add New Room') }}
	</div>
		
		<table class="table">
			<tr>
				<th>Label</th>
				<th>Capacity</th>
				<th>Building</th>
				<th>Address</th>
				<th>Action</th>
			</tr>
			
			@foreach($rooms as $room)
				<tr>
					<td>{{ $room->label }}</td>
					<td>{{ $room->capacity }}</td>
					<td>{{ $room->building }}</td>
					<td>{{ $room->address }}</td>
					<td>{{ HTML::linkRoute('edit_room', 'Edit', $room->id) }}</td>	
				</tr>
		    @endforeach
			
			
		</table>
@endsection
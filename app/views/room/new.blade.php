@extends('commons.default')

@section('content')
	<h1>New Room</h1>

	@if($errors->has())
    	<p>The following error has occur.</p>
    	<ul class="form_errors">
    		{{ $errors->first('title', '<li>:message</li>') }}
    		{{ $errors->first('capacity', '<li>:message</li>') }}
    		{{ $errors->first('address', '<li>:message</li>') }}
    		{{ $errors->first('building', '<li>:message</li>') }}
    	</ul>
    @endif


    {{ Form::open(array('route' =>'create_room', 'method' =>'POST')) }}

        	<p>
	            {{ Form::label('label', 'Room label') }} <br />
	            {{ Form::text('label', Input::old('label')) }}
	        </p>

	        <p>
	            {{ Form::label('capacity', 'capacity') }} <br />
	            {{ Form::text('capacity'), Input::old('capacity') }}
	        </p>

	        <p>
	            {{ Form::label('address', 'Address') }} <br />
	            {{ Form::text('address'), Input::old('address') }}
	        </p>

	        <p>
	            {{ Form::label('building', 'Building') }} <br />
	            {{ Form::text('building'), Input::old('building') }}
	        </p>

	        <p>{{ Form::submit('Add room') }}</p>

    {{ Form::close() }}
@endsection
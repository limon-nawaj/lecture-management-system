@extends('commons.default')

@section('content')
    <h1> {{ ucfirst($lecture->title) }}</h1>
    <p>
    	{{ $lecture->start_time }}
    </p>	
    <p>
    	{{ e($lecture->overview) }}
    </p>

    	@if($lectureDate->count() >0)
    		@if($lecture_delegate <= $lecture->room->capacity)
				@if(Auth::check())
				    @if(Auth::user()->role=='delegate')
					    {{ Form::open(array('route' =>'create_lecture_delegate', 'method' =>'POST')) }}
					    	{{ Form::hidden('lecture_id', $lecture->id) }}
					    	{{ Form::hidden('delegate_id', Auth::user()->id) }}
					    	<p>{{ Form::submit('Apply') }}</p>
					    {{ Form::close() }}
					@endif
				@else
					<p>you need to login first to apply this lecture</p>
				@endif
			@else
				<p>No sit available in this lecture</p>
			@endif
    	@else
    		<p>Lectures aren't available</p>
    	@endif
    	
		
	
@endsection
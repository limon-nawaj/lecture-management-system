@extends('commons.default')

@section('content')
	<div style="padding: 10px;">
	@if(Auth::user()->role=='organizer')
		{{ HTML::linkRoute('add_lecture', 'Add New Lecture') }}
	@endif
		
	</div>
		
		<table class="table">
			<tr>
				<th>Title</th>
				<th>Oganizer</th>
				<th>Presenter</th>
				<th>Room</th>
				<th>Start Date</th>
				<th>End Date</th>
				
				@if(Auth::user()->role=='organizer')
					<th>Action</th>
				@endif
			</tr>
			
			@foreach($lectures as $lecture)
				<tr>
					<td>{{ $lecture->title }}</td>
					<td>{{ $lecture->organizer->first_name }} {{ $lecture->organizer->last_name }}</td>
					<td>{{ $lecture->presenter->first_name }} {{ $lecture->presenter->last_name }}</td>
					<td>{{ $lecture->room->label }}</td>
					<td>{{ $lecture->start_time }}</td>
					<td>{{ $lecture->end_time }}</td>
					@if(Auth::user()->role=='organizer')
						<td>{{ HTML::linkRoute('edit_lecture', 'Edit', $lecture->id) }}</td>	
					@endif	
				</tr>
		    @endforeach
			
			
		</table>
@endsection
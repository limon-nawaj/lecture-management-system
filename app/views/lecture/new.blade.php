@extends('commons.default')

@section('content')
    <h1>Register</h1>

    @if($errors->has())
    	<p>The following error has occur.</p>
    	<ul class="form_errors">
    		
    	</ul>
    @endif

    {{ Form::open(array('route' =>'create_lecture', 'method' =>'POST')) }}

        {{ Form::hidden('organizer_id', Auth::user()->id) }}

        <p>
            {{ Form::label('title', 'Title') }} <br />
            {{ Form::text('title', Input::old('title')) }}
        </p>

        <p>
            {{ Form::label('presenter_id', 'Presenter') }} <br />
            {{ Form::select('presenter_id', $presenter, Input::old('presenter_id'))}}
        </p>

        <p>
            {{ Form::label('room_id', 'Room') }} <br />
            {{ Form::select('room_id', $room, Input::old('room_id'))}}
        </p>

        <p>
            {{ Form::label('start_time', 'Start time') }} <br />
            {{ Form::text('start_time', Input::old('start_time')) }}
        </p>

        <p>
            {{ Form::label('end_time', 'End time') }} <br />
            {{ Form::text('end_time', Input::old('end_time')) }}
        </p>

        <p>
            {{ Form::label('overview', 'Overview') }} <br />
            {{ Form::text('overview', Input::old('overview')) }}
        </p>


        <p>{{ Form::submit('Add Lecture') }}</p>

    {{ Form::close() }}
@endsection
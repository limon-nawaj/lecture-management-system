@extends('commons.default')

@section('content')
    <h1>Register</h1>

    @if($errors->has())
    	<p>The following error has occur.</p>
    	<ul class="form_errors">
    		{{ $errors->first('username', '<li>:message</li>') }}
    		{{ $errors->first('password', '<li>:message</li>') }}
    		{{ $errors->first('password_confirmation', '<li>:message</li>') }}
    	</ul>
    @endif

    {{ Form::open(array('route' =>'create_staff', 'method' =>'POST')) }}

        <p>
            {{ Form::label('username', 'Username') }} <br />
            {{ Form::text('username', Input::old('username')) }}
        </p>

        <p>
            {{ Form::label('password', 'Password') }} <br />
            {{ Form::password('password') }}
        </p>

        <p>
            {{ Form::label('password_confirmation', 'Password Confirmation') }} <br />
            {{ Form::password('password_confirmation') }}
        </p>


        @if($staffs->isEmpty())
            {{ Form::hidden('role', 'admin') }}
        @else
            {{ Form::hidden('role', 'staff') }}
        @endif

        <p>
            {{ Form::hidden('user_id') }}
        </p>

        <p>
            {{ Form::label('first_name', 'First Name') }} <br />
            {{ Form::text('first_name'), Input::old('first_name') }}
        </p>

        <p>
            {{ Form::label('last_name', 'Last Name') }} <br />
            {{ Form::text('last_name'), Input::old('last_name') }}
        </p>

        <p>
            {{ Form::label('email', 'Email') }} <br />
            {{ Form::text('email'), Input::old('email') }}
        </p>

        <p>
            {{ Form::label('phone', 'Phone') }} <br />
            {{ Form::text('phone'), Input::old('phone') }}
        </p>

        <p>
            {{ Form::label('address', 'Address') }} <br />
            {{ Form::text('address'), Input::old('address') }}
        </p>

        <p>{{ Form::submit('Add user') }}</p>

    {{ Form::close() }}
@endsection
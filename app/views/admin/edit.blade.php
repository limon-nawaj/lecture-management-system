@extends('commons.default')

@section('content')
	<h2>Edit Staff</h2>
 
        {{ Form::model($staff, array('method' => 'put', 'route' => array('update_staff', $staff->id))) }}
         

        <div class="control-group">
            {{ Form::label('first_name', 'First Name') }} 
            <div class="controls">
            	{{ Form::text('first_name'), Input::old('first_name') }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('last_name', 'Last Name') }} 
            <div class="controls">
                {{ Form::text('last_name'), Input::old('last_name') }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('email', 'Email') }} 
            <div class="controls">
                {{ Form::text('email'), Input::old('email') }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('phone', 'Phone') }} 
            <div class="controls">
                {{ Form::text('phone'), Input::old('phone') }}
            </div>
        </div>

        <div class="control-group">
            {{ Form::label('address', 'Address') }} 
            <div class="controls">
                {{ Form::text('address'), Input::old('address') }}
            </div>
        </div>

        <p>{{ Form::submit('Add user') }}</p>
 
        {{ Form::close() }}
@endsection
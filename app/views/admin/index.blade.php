@extends('commons.default')

@section('content')

<div style="padding: 10px;">
	{{ HTML::linkRoute('staff_registration', 'Add new Staff') }}
</div>
	
	<table class="table">
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Address</th>
			<th>Action</th>
		</tr>
		
		@foreach($staffs as $staff)
			<tr>
				<td>{{ $staff->first_name }}</td>
				<td>{{ $staff->last_name }}</td>
				<td>{{ $staff->phone }}</td>
				<td>{{ $staff->email }}</td>
				<td>{{ $staff->address }}</td>
				<td>{{ HTML::linkRoute('edit_staff', 'Edit', $staff->id) }}</td>	
			</tr>
	    @endforeach
		
		
	</table>

@endsection
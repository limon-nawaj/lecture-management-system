@extends('commons.default')

@section('content')
	<div style="padding: 10px;">
	{{ HTML::linkRoute('add_presenter', 'Add presenter') }}
	</div>
		
		<table class="table">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Address</th>
				<th>Action</th>
			</tr>
			
			@foreach($presenters as $presenter)
				<tr>
					<td>{{ $presenter->first_name }}</td>
					<td>{{ $presenter->last_name }}</td>
					<td>{{ $presenter->phone }}</td>
					<td>{{ $presenter->email }}</td>
					<td>{{ $presenter->address }}</td>
					<td>{{ HTML::linkRoute('edit_presenter', 'Edit', $presenter->id) }}</td>	
				</tr>
		    @endforeach
			
			
		</table>
@endsection
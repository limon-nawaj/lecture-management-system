@extends('commons.default')

@section('content')
	<div style="padding: 10px;">
	{{ HTML::linkRoute('add_organizer', 'Add Organizer') }}
	</div>
		
		<table class="table">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Address</th>
				<th>Action</th>
			</tr>
			
			@foreach($organizers as $organizer)
				<tr>
					<td>{{ $organizer->first_name }}</td>
					<td>{{ $organizer->last_name }}</td>
					<td>{{ $organizer->phone }}</td>
					<td>{{ $organizer->email }}</td>
					<td>{{ $organizer->address }}</td>
					<td>{{ HTML::linkRoute('edit_organizer', 'Edit', $organizer->id) }}</td>	
				</tr>
		    @endforeach
			
			
		</table>
@endsection
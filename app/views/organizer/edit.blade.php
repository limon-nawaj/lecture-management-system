@extends('commons.default')

@section('content')
	<h2>Edit Organizer</h2>
 
        {{ Form::model($organizer, array('method' => 'put', 'route' => array('update_organizer', $organizer->id))) }}
         	<p>
	            {{ Form::label('first_name', 'First Name') }} <br />
	            {{ Form::text('first_name'), Input::old('first_name') }}
	        </p>

	        <p>
	            {{ Form::label('last_name', 'Last Name') }} <br />
	            {{ Form::text('last_name'), Input::old('last_name') }}
	        </p>

	        <p>
	            {{ Form::label('email', 'Email') }} <br />
	            {{ Form::text('email'), Input::old('email') }}
	        </p>

	        <p>
	            {{ Form::label('phone', 'Phone') }} <br />
	            {{ Form::text('phone'), Input::old('phone') }}
	        </p>

	        <p>
	            {{ Form::label('address', 'Address') }} <br />
	            {{ Form::text('address'), Input::old('address') }}
	        </p>

	        <p>{{ Form::submit('Add user') }}</p>
        {{ Form::close() }}
@endsection
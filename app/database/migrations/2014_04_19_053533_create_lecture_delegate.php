<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLectureDelegate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lecture_delegates', function($table){
			$table->increments('id');
			$table->integer('lecture_id');
			$table->integer('delegate_id');
			$table->timestamps();
			// $table->unique(array('lecture_id', 'delegate_id'));
			// $table->primary(array('lecture_id', 'delegate_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lecture_delegates');
	}

}

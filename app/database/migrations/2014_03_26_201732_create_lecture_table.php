<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLectureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lectures', function($table){
			$table->increments('id');
			$table->string('title');
			$table->integer('organizer_id');
			$table->integer('presenter_id');
			$table->integer('room_id');
			$table->string('overview');
			// $table->date('start_date');
			// $table->date('end_date');
			$table->datetime('start_time');
			$table->datetime('end_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lectures');
	}

}

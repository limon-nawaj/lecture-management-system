<?php 

/**
* 
*/
class Room extends BaseModel
{
	public static $unguarded = true;
	protected $table = 'rooms';
	public static $rules = array(
		'label' 	=> 'required', 
		'capacity' 	=> 'required|integer',
		'address' 	=> 'required',
		'building' 	=> 'required'
	);

	public function lecture()
	{
		return $this->hasMany('Lecture');
	}
}
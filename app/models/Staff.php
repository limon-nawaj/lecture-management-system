<?php 

/**
* 
*/
class Staff extends BaseModel
{
	public static $unguarded = true;
	
	protected $table = 'staffs';
	public static $rules = array(
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|unique:presenters'
	);

	public function user()
	{
		return $this->belongs_to('User');
	}
}
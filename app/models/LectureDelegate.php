<?php 



class LectureDelegate extends BaseModel
{
	public static $unguarded = true;
	protected $table = 'lecture_delegates';
	public static $rules = array(
		'lecture_id' => 'required|unique_with:lecture_delegates, delegate_id',
		'delegate_id' => 'required'
	);
}
<?php 

/**
* 
*/
class Organizer extends BaseModel
{
	public static $unguarded = true;
	
	protected $table = 'organizers';
	public static $rules = array(
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|unique:presenters'
	);

	public function user()
	{
		return $this->belongs_to('User');
	}

	public function lecture()
	{
		return $this->hasMany('Lecture');
	}
}
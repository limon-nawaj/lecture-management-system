<?php 

/**
* 
*/
class Lecture extends BaseModel
{
	public static $unguarded = true;
	protected $table = 'lectures';
	public static $rules = array();

	public function room()
	{
		return $this->belongsTo('Room');
	}

	public function presenter()
	{
		return $this->belongsTo('Presenter');
	}

	public function organizer()
	{
		return $this->belongsTo('Organizer');
	}

	public function getDates()
	{
	    return array('created_at', 'updated_at', 'start_time');
	}
}
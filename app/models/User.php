<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static $unguarded = true;
	protected $table = 'users';

	public static $rules = array(
		'username' => 'required|unique:users|min:4', 
		'password' => 'required|alpha_num|between:4, 8|confirmed',
		'password_confirmation' => 'required|alpha_num|between:4, 8'
	);

	protected $hidden = array('password');

	public function staff()
	{
		return $this->belongs_to('Staff');
	}

	public function organizer()
	{
		return $this->belongs_to('Organizer');
	}

	public function presenter()
	{
		return $this->belongs_to('Presenter');
	}

	
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

	
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

}